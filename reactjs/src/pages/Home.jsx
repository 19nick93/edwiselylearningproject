import React, { Fragment } from 'react'
import Header from '../components/header/Header';
import HeroSection from '../components/header/Hero-Section/HeroSection';
import Company from '../components/Company-section/Company';
import AboutUs from '../components/About-us/AboutUs';
import Courses from '../components/Courses-section/Courses';
import ChooseUs from '../components/Choose-us/ChooseUs';
import Features from '../components/Feature-section/Features';
import FreeCourse from '../components/Free-course-section/FreeCourse';
import Footer from '../components/Footer/Footer';
const Home = () => {
  return <Fragment>
    <Header />
    <HeroSection/>
    <Company/>
    <AboutUs/>
    <Courses/>
    <ChooseUs/>
    <Features/>
    <FreeCourse/>
    <Footer/>
  </Fragment>

}

export default Home;

